/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
import static ca.sheridancollege.softwarefundamentals.pickacard.Card.SUITS;
import java.util.Scanner;
public class CardTrick {
    
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Card[] magicHand = new Card[7];
        
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            c.setValue(c.randomNumber());
            c.setSuit(Card.SUITS[c.randomSuit()]);
            magicHand[i] = c;
        }
        
        
            System.out.println("Choose any card");
            System.out.println("\n" + "Pick the value of card");
            System.out.println("Choose value between 1 to 13");
            int inputValue = scanner.nextInt();
            
            System.out.println("\n" + "Pick the suit of card");
            System.out.println("For choosing suit, choose value accordingly" + "\n" +
                                "For Hearts = 1 , For Diamonds = 2, For Spades = 3, For Clubs = 4");
            int inputSuit = scanner.nextInt();
            
            String newInputSuit ="";
        switch (inputSuit) {
            case 1:
                newInputSuit = SUITS[0];
                break;
            case 2:
                newInputSuit = SUITS[1];
                break;
            case 3:
                newInputSuit = SUITS[2];
                break;
            case 4:
                newInputSuit = SUITS[3];
                break;
            default:
                System.out.println("\n" + "Error: Choose between 1 to 4");
                break;
        }
            System.out.println("\n" + "The card you chose is " + inputValue + " of " + newInputSuit);
            
            
           
           // c.setValue(insert call to random number generator here)
           // c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
        
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
    }
    
}
